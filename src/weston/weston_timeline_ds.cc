/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Aguado Puig, Quim <quim.aguado@collabora.com>
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <poll.h>

#include "weston_timeline_ds.h"
#include "parser.h"


namespace pps::weston
{

wl_display* TimelineDataSource::display = nullptr;
wl_registry* TimelineDataSource::registry = nullptr;
weston_debug_stream_v1* TimelineDataSource::stream = nullptr;
State TimelineDataSource::state = State::Stop;
bool TimelineDataSource::stream_complete = false;

TimelineBuffer TimelineDataSource::json_buffer = TimelineBuffer();

Parser TimelineDataSource::parser = Parser();
std::vector<WestonObject::Id> TimelineDataSource::tracks = {};

int TimelineDataSource::read_fd = -1;
int TimelineDataSource::write_fd = -1;


void TimelineDataSource::reset()
{
	assert( stream && "Stream should be valid" );
	weston_debug_stream_v1_destroy( stream );
	stream = nullptr;
	PERFETTO_ILOG( "Stream %s destroyed", get_stream_name() );

	// Wait for server to close all files
	assert( display && "Display should be valid" );
	wl_display_roundtrip( display );

	assert( registry && "Registry should be valid" );
	wl_registry_destroy( registry );
	registry = nullptr;
	PERFETTO_ILOG( "Registry destroyed" );

	wl_display_disconnect( display );
	display = nullptr;

	assert( write_fd >= 0 && "Write pipe should be open" );
	close( write_fd );
	write_fd = -1;

	assert( read_fd >= 0 && "Read pipe should be open" );
	close( read_fd );
	read_fd = -1;

	parser = Parser();
	tracks.clear();

	state = State::Stop;
}


TimelineBuffer TimelineDataSource::read_timeline( const int fd )
{
	auto buffer = TimelineBuffer( 512 );
	ssize_t read_count = 0;

	// Process incoming events before attempting to read them
	assert( display && "Display should be initialized" );
	wl_display_roundtrip( display );

	assert( fd >= 0 && "File should be open" );

	struct pollfd fds[1];
	fds[0].fd = fd;
	fds[0].events = POLLIN;
	fds[0].revents = 0;

	int retpoll = poll( fds, 1, 0 );
	if ( retpoll < 0 )
	{
		PERFETTO_FATAL( "Poll failed" );
	}
	if ( retpoll == 0 )
	{
		// Timeout
	}
	else if ( retpoll & POLLIN == 0 )
	{
		// There is not data to read
	}
	else
	{
		read_count = read(
			fd,
			buffer.data(),
			buffer.size() - 1 );

		if ( read_count < 0 )
		{
			PERFETTO_FATAL( "Error reading pipe" );
		}
		else if ( read_count == 0 )
		{
			PERFETTO_ELOG( "EOF" );
		}
	}

	// Mark end of string
	buffer[read_count] = 0;

	if ( read_count > 0 )
	{
		// Get rid of unused memory
		buffer.resize( read_count + 1 );
	}
	else
	{
		buffer.clear();
	}
	
	return buffer;
}


/// @brief An track event type can be instant, slice begin, or slice end
/// @return The event type according to the weston timepoint
perfetto::protos::pbzero::TrackEvent::Type
track_from_timepoint( WestonTimepoint::Type type )
{
	using namespace perfetto::protos::pbzero;

	switch ( type )
	{
	case WestonTimepoint::Type::CORE_REPAINT_POSTED:
	case WestonTimepoint::Type::CORE_REPAINT_REQ:
		return TrackEvent::TYPE_INSTANT;
	/// @todo Not always a commit damage is followed by a flush
	/// We can store last commit damage of a surface and generate a slice
	/// when we get a flush.
	/// If we instead get another commit, we generate an instant event for
	/// the previous commit, and overwrite last commit with the new one.
	case WestonTimepoint::Type::CORE_COMMIT_DAMAGE:
	case WestonTimepoint::Type::CORE_REPAINT_BEGIN:
	case WestonTimepoint::Type::CORE_REPAINT_ENTER_LOOP:
	/// @todo Do not use render_gpu for now, as it is incorrect
	case WestonTimepoint::Type::RENDERER_GPU_BEGIN:
		return TrackEvent::TYPE_SLICE_BEGIN;
	case WestonTimepoint::Type::CORE_FLUSH_DAMAGE:
	case WestonTimepoint::Type::CORE_REPAINT_FINISHED:
	case WestonTimepoint::Type::CORE_REPAINT_EXIT_LOOP:
	case WestonTimepoint::Type::RENDERER_GPU_END:
		return TrackEvent::TYPE_SLICE_END;
	default:
		PERFETTO_ELOG( "Timepoint type not handled: %d", static_cast<int32_t>( type ) );
		return TrackEvent::TYPE_UNSPECIFIED;
	}
}


uint64_t TimelineDataSource::get_track_uuid( const WestonTimepoint& event, TraceContext& ctx )
{
	/// @todo We need to create ProcessTracks for outputs and ThreadTracks
	/// for (i) timepoints on the output itself and (ii) core_commits of
	/// surfaces onto that output

	// An event refers to a surface or an output
	const WestonObject* weston_object = nullptr;
	if ( event.surface )
	{
		weston_object = parser.get_object( event.surface );
	}
	else if ( event.output )
	{
		weston_object = parser.get_object( event.output );
	}
	assert( weston_object && "Event refers to invalid objects" );

	// Create track for this object if not already there
	auto tracks_it = std::find( std::begin( tracks ), std::end( tracks ), weston_object->id );
	if ( tracks_it == std::end( tracks ) )
	{
		auto packet = ctx.NewTracePacket();
		auto track = packet->set_track_descriptor();
		PERFETTO_LOG( "Track %u - %s", weston_object->id, weston_object->name.c_str() );
		track->set_name( weston_object->name + " - " + weston_object->description );
		track->set_uuid( weston_object->id );
		tracks.emplace_back( weston_object->id );
	}

	return weston_object->id;
}


void TimelineDataSource::trace_callback( TraceContext ctx )
{
	// Read bytes from the pype
	auto read_buffer = read_timeline( read_fd );
	if ( read_buffer.empty() )
	{
		return; // Nothing to do
	}
	
	// Append to the current json buffer, as it may be previous unparsed bytes
	json_buffer.reserve( json_buffer.size() + read_buffer.size() );
	json_buffer.insert(
		std::end( json_buffer ),
		std::begin( read_buffer ),
		std::end( read_buffer )
	);

	auto [events, chars_parsed] = parser.parse( json_buffer );

	// Remove parsed chars and '0'
	// Unparsed characters will be reused on the next trace callback
	std::rotate(
		std::begin( json_buffer ),
		std::begin( json_buffer ) + chars_parsed,
		std::end( json_buffer ) );
	json_buffer.resize( json_buffer.size() - chars_parsed - 1 );

	if ( chars_parsed == 0 || events.empty() )
	{
		return; // Skip sending packet
	}

	// Send a packet for each event
	for ( auto& event : events )
	{
		// Note: Do not reorder as it may create a new trace packet
		auto track_uuid = get_track_uuid( event, ctx );

		auto packet = ctx.NewTracePacket();
		auto event_ns = event.ts.tv_sec * 1000000000 + event.ts.tv_nsec;
		packet->set_timestamp( event_ns );

		auto track_event = packet->set_track_event();
		track_event->set_name( event.name );
		track_event->set_type( track_from_timepoint( event.type ) );
		track_event->set_track_uuid( track_uuid );
		track_event->add_categories( "weston" );
	}
}


void TimelineDataSource::sample()
{
	Trace( trace_callback );
}


void TimelineDataSource::debug_available( void* data, weston_debug_v1* debug, const char* name, const char* description )
{
	auto datasource = static_cast<TimelineDataSource*>( data );

	if ( strcmp( name, get_stream_name() ) == 0 )
	{
		datasource->stream_exists = true;
		PERFETTO_ILOG( "Debug stream %s found", name );
	}
}


const weston_debug_v1_listener TimelineDataSource::debug_listener = {
	TimelineDataSource::debug_available,
};


void TimelineDataSource::global_add( void* data, wl_registry* registry, uint32_t id, const char* interface, uint32_t version )
{
	if ( strcmp( interface, weston_debug_v1_interface.name ) == 0 )
	{
		auto datasource = static_cast<TimelineDataSource*>( data );
		assert( !datasource->debug_iface && "Debug interface should not be initialized" );
		datasource->debug_iface = static_cast<weston_debug_v1*>(
			wl_registry_bind( registry, id, &weston_debug_v1_interface, version ) );
		weston_debug_v1_add_listener( datasource->debug_iface, &debug_listener, datasource );
	}
}


static void global_remove( void *data, wl_registry* registry, uint32_t id )
{
	/// @todo Fail if debug interface is removed from registry
}


const wl_registry_listener TimelineDataSource::registry_listener = {
	.global = TimelineDataSource::global_add,
	.global_remove = global_remove
};


void TimelineDataSource::OnSetup( const SetupArgs& args )
{
	assert( !display && "Display should not be initialized" );
	if ( display = wl_display_connect( nullptr ) )
	{
		auto version = wl_display_get_version( display );
		PERFETTO_LOG( "Connected to Wayland server v%u", version );
	}
	else
	{
		PERFETTO_FATAL( "Failed to create Wayland display" );
	}

	assert( !registry && "Registry should not be initialized" );
	registry = wl_display_get_registry( display );
	wl_registry_add_listener( registry, &registry_listener, this );

	wl_display_roundtrip( display );

	if ( !debug_iface )
	{
		PERFETTO_FATAL( "Global %s does not exists in the registry. "
			"Make sure weston is running with the --debug flag",
			weston_debug_v1_interface.name );
	}

	wl_display_roundtrip( display );

	if ( !stream_exists )
	{
		PERFETTO_FATAL( "Stream %s not found", get_stream_name() );
	}
	
	PERFETTO_LOG( "Initialization finished" );
}


void TimelineDataSource::handle_stream_complete( void* data, weston_debug_stream_v1* stream )
{
	stream_complete = true;
	weston_debug_stream_v1_destroy( stream );
	stream = nullptr;
	PERFETTO_FATAL( "Stream %s completed", get_stream_name() );
}


void TimelineDataSource::handle_stream_failure( void* data, weston_debug_stream_v1* stream, const char* msg )
{
	stream_complete = true;
	weston_debug_stream_v1_destroy( stream );
	stream = nullptr;
	PERFETTO_FATAL( "Stream %s failed", get_stream_name() );
}


const weston_debug_stream_v1_listener TimelineDataSource::stream_listener = {
	TimelineDataSource::handle_stream_complete,
	TimelineDataSource::handle_stream_failure,
};


void TimelineDataSource::OnStart( const StartArgs& args )
{
	PERFETTO_ILOG( "Starting %s stream", get_stream_name() );

	assert( read_fd == -1 && "Read pipe should not be initialized at this point" );
	assert( write_fd == -1 && "Write pipe should not be initialized at this point" );
	// Create a pipe for interprocess communication
	// Weston debug protocol will dump onto the write end of the pipe
	// This data-source will read the data as it comes.
	int fd[2];
	if ( pipe( fd ) == -1 )
	{
		PERFETTO_FATAL( "Can not create a pipe to read the stream" );
	}
	PERFETTO_LOG( "Created stream pipe" );
	read_fd = fd[0];
	write_fd = fd[1];

	assert( stream == nullptr && "Stream should not be initialized at this point" );
	assert( debug_iface && "Debug interface should be valid" );
	stream = weston_debug_v1_subscribe( debug_iface, get_stream_name(), write_fd );
	weston_debug_stream_v1_add_listener( stream, &stream_listener, nullptr );

	weston_debug_v1_destroy( debug_iface );
	debug_iface = nullptr;

	state = State::Start;
	PERFETTO_LOG( "OnStart finished" );
}


void TimelineDataSource::OnStop( const StopArgs& args )
{
	PERFETTO_LOG( "Parse errors: %lu", parser.get_error_count() );
	state = State::Stop;
	auto stop_closure = args.HandleStopAsynchronously();
	Trace( [] ( TraceContext ctx ) {
		PERFETTO_LOG( "Tracing lambda called while stopping" );
		ctx.Flush();
		reset();
	} );
	stop_closure();
	PERFETTO_LOG( "OnStop finished" );
}


} // namespace pps::weston
