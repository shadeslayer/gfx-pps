# Copyright (c) 2020 Collabora, Ltd.
# Author: Antonio Caggiano <antonio.caggiano@collabora.com>
#
# SPDX-License-Identifier: GPL-2.0-only

find_program( WAYLAND_SCANNER wayland-scanner )

set( WESTON_DEBUG_XML ${CMAKE_SOURCE_DIR}/dep/weston/protocol/weston-debug.xml )
# Weston debug generated files
set( GEN_WESTON_C weston_debug_client_protocol.c )
set( GEN_WESTON_H weston_debug_client_protocol.h )

add_custom_command(
	OUTPUT ${GEN_WESTON_C}
	OUTPUT ${GEN_WESTON_H}
	COMMAND ${WAYLAND_SCANNER} private-code ${WESTON_DEBUG_XML} ${GEN_WESTON_C}
	COMMAND ${WAYLAND_SCANNER} client-header ${WESTON_DEBUG_XML} ${GEN_WESTON_H}
)

# Weston debug lib
add_library( weston-debug ${GEN_WESTON_C} )
target_include_directories( weston-debug PUBLIC ${CMAKE_CURRENT_BINARY_DIR} )

# Find wayland client
find_package( Wayland REQUIRED )
find_package( RapidJSON REQUIRED )

# Weston timeline data source
set( WT_SOURCES
	${CMAKE_CURRENT_SOURCE_DIR}/main.cc
	${CMAKE_CURRENT_SOURCE_DIR}/parser.cc
	${CMAKE_CURRENT_SOURCE_DIR}/weston_timeline_ds.cc )

add_executable( producer-weston ${WT_SOURCES} )
target_link_libraries( producer-weston weston-debug wayland-client pps )
target_include_directories( producer-weston PRIVATE ${RAPIDJSON_INCLUDE_DIRS} )
