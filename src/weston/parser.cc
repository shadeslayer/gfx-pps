/*
 * Copyright (c) 2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include "parser.h"

#include <rapidjson/document.h>
#include <perfetto.h>

namespace pps::weston
{


/// @return Conversion of a string to a weston object type
WestonObject::Type object_from_string( const std::string_view& name )
{
	if ( name == "weston_surface" )
	{
		return WestonObject::Type::SURFACE;
	}
	else if ( name == "weston_output" )
	{
		return WestonObject::Type::OUTPUT;
	}

	PERFETTO_ELOG( "Unknown weston object type: %s", name.data() );
	return WestonObject::Type::UNDEFINED;
}


/// @return Conversion of a string to a weston timepoint type
WestonTimepoint::Type timepoint_from_string( const std::string_view& name )
{
	if ( name == "core_commit_damage" )
	{
		return WestonTimepoint::Type::CORE_COMMIT_DAMAGE;
	}
	else if ( name == "core_flush_damage" )
	{

		return WestonTimepoint::Type::CORE_FLUSH_DAMAGE;
	}
	else if ( name == "core_repaint_begin" )
	{
		return WestonTimepoint::Type::CORE_REPAINT_BEGIN;
	}
	else if ( name == "core_repaint_finished" )
	{
		return WestonTimepoint::Type::CORE_REPAINT_FINISHED;
	}
	else if ( name == "core_repaint_posted" )
	{
		return WestonTimepoint::Type::CORE_REPAINT_POSTED;
	}
	else if ( name == "core_repaint_enter_loop" )
	{
		return WestonTimepoint::Type::CORE_REPAINT_ENTER_LOOP;
	}
	else if ( name == "core_repaint_exit_loop" )
	{
		return WestonTimepoint::Type::CORE_REPAINT_EXIT_LOOP;
	}
	else if ( name == "core_repaint_req" )
	{
		return WestonTimepoint::Type::CORE_REPAINT_REQ;
	}
	else if ( name == "renderer_gpu_begin" )
	{
		return WestonTimepoint::Type::RENDERER_GPU_BEGIN;
	}
	else if ( name == "renderer_gpu_end" )
	{
		return WestonTimepoint::Type::RENDERER_GPU_END;
	}

	PERFETTO_ELOG( "Unknown weston timepoint type: %s", name.data() );
	return WestonTimepoint::Type::UNDEFINED;
}


/// @return The name of a weston timepoint
std::string name_from_timepoint( const WestonTimepoint::Type type )
{
	switch ( type )
	{
		case WestonTimepoint::Type::CORE_COMMIT_DAMAGE:
		case WestonTimepoint::Type::CORE_FLUSH_DAMAGE:
			return "core_damage";
		case WestonTimepoint::Type::CORE_REPAINT_BEGIN:
		case WestonTimepoint::Type::CORE_REPAINT_FINISHED:
			return "core_repaint";
		case WestonTimepoint::Type::CORE_REPAINT_POSTED:
			return "core_repaint_posted";
		case WestonTimepoint::Type::CORE_REPAINT_ENTER_LOOP:
		case WestonTimepoint::Type::CORE_REPAINT_EXIT_LOOP:
			return "core_repaint_loop";
		case WestonTimepoint::Type::CORE_REPAINT_REQ:
			return "core_repaint_req";
		case WestonTimepoint::Type::RENDERER_GPU_BEGIN:
		case WestonTimepoint::Type::RENDERER_GPU_END:
			return "renderer_gpu";
		default:
			PERFETTO_ELOG( "Unknown name weston timepoint type %d", static_cast<int>( type ) );
			return "unknown";
	}
}

std::string get_string( const rapidjson::Document& doc, const char* name )
{
	if ( doc.HasMember( name ) && doc[name].IsString() )
	{
		return doc[name].GetString();
	}

	return "unknown";
}


/// @brief Parses a json respresentation of a WestonObject
/// @return A newly created weston object
WestonObject parse_object( const rapidjson::Document& doc )
{
	WestonObject ret;

	assert( doc.HasMember( "id" ) && "Weston object should have an id" );
	ret.id = doc["id"].GetUint();

	auto type_str = get_string( doc, "type" );
	ret.type = object_from_string( type_str );
	ret.name = get_string( doc, "name" );
	ret.description = get_string( doc, "desc" );
	PERFETTO_ILOG(
		"%s %u - %s: %s",
		type_str.c_str(),
		ret.id,
		ret.name.c_str(),
		ret.description.c_str() );

	return ret;
}


/// @brief Parses a json respresentation of a @ref WestonTimepoint
/// @return A newly created weston timepoint
WestonTimepoint parse_timepoint( const rapidjson::Document& doc )
{
	WestonTimepoint ret;

	assert( doc.HasMember( "T" ) && "Weston timepoint should have a T" );
	auto& tvalue = doc["T"];
	assert( tvalue.IsArray() && "T should be an array" );
	assert( tvalue.Size() == 2 && "T size should be 2" );
	ret.ts.tv_sec = tvalue[0].GetInt64();
	ret.ts.tv_nsec = tvalue[1].GetInt64();

	ret.type = timepoint_from_string( get_string( doc, "N" ) );
	ret.name = name_from_timepoint( ret.type );

	// Check if it refers to a surface
	if ( doc.HasMember( "ws" ) )
	{
		auto& object_id = doc["ws"];
		assert( object_id.IsUint() && "Weston surface ID should be uint" );
		ret.surface = object_id.GetUint();
	}

	// Check if it refers to an output
	if ( doc.HasMember( "wo" ) )
	{
		auto& object_id = doc["wo"];
		assert( object_id.IsUint() && "Weston surface ID should be uint" );
		ret.output = object_id.GetUint();
	}

	return ret;
}


const WestonObject* Parser::get_object( const WestonObject::Id id ) const
{
	// Id 0 is not defined, as they start from 1
	if ( id && id <= objects.size() )
	{
		return &objects[id - 1];
	}

	return nullptr;
}


void Parser::add_or_update( WestonObject&& obj )
{
	assert( obj.id && "Id 0 is not valid for weston objects" );

	// Weston object ids start from 1
	if ( obj.id <= objects.size() )
	{
		// Update
		auto& old_obj = objects[obj.id - 1];
		assert( old_obj.id == obj.id && "Updated weston object id mismatch" );
		old_obj = std::move( obj );
	}
	else // Add
	{
		assert( ( obj.id == ( objects.size() + 1 ) ) && "New weston object id mismatch" );
		objects.emplace_back( std::move( obj ) );
	}
}


std::pair<std::vector<WestonTimepoint>, size_t> Parser::parse( const TimelineBuffer& buffer )
{
	std::vector<WestonTimepoint> ret;
	size_t chars_parsed = 0;

	if ( buffer.empty() )
	{
		return { ret, chars_parsed };
	}

	using namespace rapidjson;

	auto jstream = StringStream( buffer.data() );
	Document doc;

	while ( true )
	{
		doc.ParseStream<kParseStopWhenDoneFlag>( jstream );

		if ( doc.HasParseError() )
		{
			if ( doc.GetParseError() == ParseErrorCode::kParseErrorDocumentEmpty )
			{
				// Stream empty
				break;
			}

			++error_count;
			break;
		}

		if ( doc.HasMember( "id" ) )
		{
			add_or_update( parse_object( doc ) );
		}
		else if ( doc.HasMember( "T" ) )
		{
			ret.emplace_back( parse_timepoint( doc ) );
		}
		else
		{
			PERFETTO_ELOG( "Unknown timeline object" );
		}

		chars_parsed = jstream.Tell();
	}

	return { ret, chars_parsed };
}


} // namespace pps::weston
