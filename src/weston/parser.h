/*
 * Copyright (c) 2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <array>
#include <vector>
#include <string>

namespace pps::weston
{


using TimelineBuffer = std::vector<char>;


struct WestonObject
{
	enum class Type
	{
		UNDEFINED,
		SURFACE,
		OUTPUT,
		MAX
	};

	using Id = uint32_t;
	Id id = 0;

	Type type;

	std::string name;
	std::string description;
};


struct WestonTimepoint
{
	enum class Type
	{
		UNDEFINED,
		// Client submitting a new frame
		CORE_COMMIT_DAMAGE,
		CORE_FLUSH_DAMAGE,
		CORE_REPAINT_BEGIN,
		CORE_REPAINT_FINISHED,
		CORE_REPAINT_POSTED,
		CORE_REPAINT_ENTER_LOOP,
		CORE_REPAINT_EXIT_LOOP,
		// Repaint has been requested
		CORE_REPAINT_REQ,
		RENDERER_GPU_BEGIN,
		RENDERER_GPU_END,
		MAX,
	};

	timespec ts;

	std::string name;

	Type type;


	WestonObject::Id output = 0;

	WestonObject::Id surface = 0;
};


class Parser
{
  public:
	/// @brief Parses a buffer containing JSON weston timeline events
	/// @return The list of event parsed from the timeline and the number of chars parsed correcly
	std::pair<std::vector<WestonTimepoint>, size_t> parse( const TimelineBuffer& buffer );

	/// @return The weston object with that ID; nullptr if not found
	const WestonObject* get_object( WestonObject::Id id ) const;

	/// @return The number of errors accumulated during all parsing
	size_t get_error_count() const { return error_count; }

  private:
	/// @brief Adds a new weston object, or updates an old one if already present
	void add_or_update( WestonObject&& obj );

	/// Keeps track of the number of errors
	size_t error_count = 0;

	/// Store for weston objects. Their index in the vector is their id
	std::vector<WestonObject> objects;
};


} // namespace pps::weston
