/*
 * Copyright (c) 2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <cstdlib>
#include <thread>

#include "weston_timeline_ds.h"


int main()
{
	using namespace pps;

	// Connects to the system tracing service
	perfetto::TracingInitArgs args;
	args.backends = perfetto::kSystemBackend;
	perfetto::Tracing::Initialize( args );

	perfetto::DataSourceDescriptor dsd;
	dsd.set_name( weston::TimelineDataSource::get_name() );
	weston::TimelineDataSource::Register( dsd );

	while ( true )
	{
		switch ( weston::TimelineDataSource::get_state() )
		{
			using namespace std::chrono_literals;

			case State::Stop:
			{
				// Just wait until it starts
				std::this_thread::sleep_for( 2ms );
				break;
			}
			case State::Start:
			{
				weston::TimelineDataSource::sample();
				/// @todo Can this time be specified by a perfetto config file?
				std::this_thread::sleep_for( 2ms );
				break;
			}
			default:
			{
				assert( false && "Invalid DataSource state" );
			}
		}
	}

	return EXIT_SUCCESS;
}
