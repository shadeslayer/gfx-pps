/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Aguado Puig, Quim <quim.aguado@collabora.com>
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <pps/pds.h>
#include "weston_debug_client_protocol.h"

#include "parser.h"

namespace pps::weston
{


class TimelineDataSource final: public perfetto::DataSource<TimelineDataSource>
{
  public:
	static const char* get_name() { return "weston.debug.timeline"; }
	static const char* get_stream_name() { return "timeline"; }

	/// @return The current state of this data-source
	static State get_state() { return state; }

	/// @brief Sample data, should be called in a loop
	static void sample();

	void OnSetup( const SetupArgs& args ) override;
	void OnStart( const StartArgs& args ) override;
	void OnStop( const StopArgs& args ) override;

	/// @brief To be called on reset state
	static void reset();

  private:

	/// Weston debug callbacks
	static void global_add( void* data, wl_registry* registry, uint32_t id, const char* interface, uint32_t version );
	static void debug_available( void* data, weston_debug_v1* debug, const char* name, const char* description );
	static void handle_stream_failure( void* data, weston_debug_stream_v1* stream, const char* msg );
	static void handle_stream_complete( void* data, weston_debug_stream_v1* stream );

	/// @brief Perfetto callback helper
	/// @return The track uuid for the event
	static uint64_t get_track_uuid( const WestonTimepoint& event, TraceContext& ctx );

	/// @brief Perfetto callback
	static void trace_callback( TraceContext ctx );

	/// @brief Reads weston json timeline
	static TimelineBuffer read_timeline( int fd );

	static State state;

	static wl_display* display;
	static wl_registry* registry;

	static const weston_debug_v1_listener debug_listener;
	static const wl_registry_listener registry_listener;
	static const weston_debug_stream_v1_listener stream_listener;
	static bool stream_complete;

	static weston_debug_stream_v1* stream;

	/// Read and write ends of a pipe
	static int read_fd;
	static int write_fd;

	/// Used to store butes read from the pipe
	static TimelineBuffer json_buffer;

	/// Used to parse a json string into weston timeline events
	static Parser parser;

	/// Use weston object ids as perfetto tracks UUIDs
	static std::vector<WestonObject::Id> tracks;

	weston_debug_v1* debug_iface = nullptr;
	bool stream_exists = false;

};


}  // namespace pps::weston
