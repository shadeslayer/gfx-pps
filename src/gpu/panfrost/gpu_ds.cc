/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include "gpu_ds.h"

#include <cerrno>
#include <cstring>
#include <thread>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <drm/panfrost_drm.h>

#include "mali_counter_names.h"

namespace pps::gpu
{

counter_t* PanfrostDataSource::counters = nullptr;
int PanfrostDataSource::card_fd = -1;

static PanfrostDataSource::State state = PanfrostDataSource::State::Stop;

PanfrostDataSource::State PanfrostDataSource::get_state()
{
	return state;
}

/// @brief Checks whether a return value is valid
/// @param res Result from a syscall
/// @param msg Message to prepend to strerror
/// @return True if ok, false otherwise
bool check( int res, const char* msg )
{
	if ( res < 0 )
	{
		char* err_msg = std::strerror( errno );
		PERFETTO_ELOG( "%s: %s", msg, err_msg );
		return false;
	}

	return true;
}

constexpr size_t count_counters()
{
	return sizeof( hardware_counters_mali_t86x ) / sizeof( hardware_counters_mali_t86x[0] );
}

constexpr size_t counters_per_block()
{
	const size_t block_count = 4;
	return count_counters() / block_count;
}

/// @brief Allocates memory to store data from GPU
void PanfrostDataSource::OnSetup( const SetupArgs& args )
{
	if ( !counters )
	{
		/// @todo Find a proper size for the buffer
		/// since `count_counters()` is not enough
		counters = allocator.allocate( 2 * count_counters() );
	}

	PERFETTO_LOG( "Initialization finished" );
}

/// @brief Enables performance counters
void PanfrostDataSource::OnStart( const StartArgs& args )
{
	if ( card_fd < 0 )
	{
		card_fd = open( card_path, O_RDONLY );
		if ( !check( card_fd, "Cannot open graphics card" ) )
		{
			PERFETTO_FATAL( "Please verify path" );
		}
	}

	drm_panfrost_perfcnt_enable perfcnt = {};
	perfcnt.enable = 1;
	perfcnt.counterset = 0;

	auto res = ioctl( card_fd, DRM_IOCTL_PANFROST_PERFCNT_ENABLE, &perfcnt );
	if ( !check( res, "Cannot enable perfcnt" ) )
	{
		if ( res == -ENOSYS )
		{
			PERFETTO_FATAL( "Please enable unstable ioctls with: modprobe panfrost unstable_ioctls=1" );
		}
		PERFETTO_FATAL( "Please verify graphics card" );
	}

	state = State::Start;

	PERFETTO_LOG( "OnStart finished" );
}

void close_callback( PanfrostDataSource::TraceContext ctx )
{
	auto packet = ctx.NewTracePacket();
	packet->Finalize();
	ctx.Flush();
	PERFETTO_LOG( "Context flushed" );
}

/// @brief Disables performance counters
void PanfrostDataSource::OnStop( const StopArgs& args )
{
	Trace( close_callback );

	drm_panfrost_perfcnt_enable perfcnt = {};
	perfcnt.enable = 0; // disable

	auto res = ioctl( card_fd, DRM_IOCTL_PANFROST_PERFCNT_ENABLE, &perfcnt );
	check( res, "Cannot disable perfcnt" );

	close( card_fd );
	card_fd = -1;

	state = State::Stop;

	PERFETTO_LOG( "OnStop finished" );
}

/// @return A string representation for the counter
const char* to_string( const Counter counter )
{
	switch( counter )
	{
	case Counter::GPU_ACTIVE: return "GPU_ACTIVE";
	case Counter::JS0_ACTIVE: return "JS0_ACTIVE";
	case Counter::JS1_ACTIVE: return "JS1_ACTIVE";
	case Counter::JS2_ACTIVE: return "JS2_ACTIVE";
	case Counter::IRQ_ACTIVE: return "IRQ_ACTIVE";
	case Counter::JS0_TASKS: return "JS0_TASKS";
	case Counter::JS1_TASKS: return "JS1_TASKS";
	case Counter::JS2_TASKS: return "JS2_TASKS";
	case Counter::FRAG_ACTIVE: return "FRAG_ACTIVE";
	case Counter::COMPUTE_ACTIVE: return "COMPUTE_ACTIVE";
	case Counter::TRIPIPE_ACTIVE: return "TRIPIPE_ACTIVE";
	case Counter::COMPUTE_TASKS: return "COMPUTE_TASKS";
	case Counter::COMPUTE_THREADS: return "COMPUTE_THREADS";
	case Counter::FRAG_PRIMITIVES: return "FRAG_PRIMITIVES";
	case Counter::FRAG_PRIMITIVES_DROPPED: return "FRAG_PRIMITIVES_DROPPED";
	case Counter::FRAG_QUADS_RAST: return "FRAG_QUADS_RAST";
	case Counter::FRAG_QUADS_EZS_TEST: return "FRAG_QUADS_EZS_TEST";
	case Counter::FRAG_QUADS_EZS_KILLED: return "FRAG_QUADS_EZS_KILLED";
	case Counter::FRAG_CYCLES_NO_TILE: return "FRAG_CYCLES_NO_TILE";
	case Counter::FRAG_THREADS: return "FRAG_THREADS";
	case Counter::FRAG_DUMMY_THREADS: return "FRAG_DUMMY_THREADS";
	case Counter::FRAG_THREADS_LZS_TEST: return "FRAG_THREADS_LZS_TEST";
	case Counter::FRAG_THREADS_LZS_KILLED: return "FRAG_THREADS_LZS_KILLED";
	case Counter::FRAG_NUM_TILES: return "FRAG_NUM_TILES";
	case Counter::FRAG_TRANS_ELIM: return "FRAG_TRANS_ELIM";
	case Counter::ARITH_WORDS: return "ARITH_WORDS";
	case Counter::LS_WORDS: return "LS_WORDS";
	case Counter::LS_ISSUES: return "LS_ISSUES";
	case Counter::LSC_READ_HITS: return "LSC_READ_HITS";
	case Counter::LSC_READ_OP: return "LSC_READ_OP";
	case Counter::LSC_WRITE_HITS: return "LSC_WRITE_HITS";
	case Counter::LSC_WRITE_OP: return "LSC_WRITE_OP";
	case Counter::LSC_ATOMIC_HITS: return "LSC_ATOMIC_HITS";
	case Counter::LSC_ATOMIC_OP: return "LSC_ATOMIC_OP";
	case Counter::LSC_LINE_FETCHES: return "LSC_LINE_FETCHES";
	case Counter::LSC_DIRTY_LINE: return "LSC_DIRTY_LINE";
	case Counter::LSC_SNOOPS: return "LSC_SNOOPS";
	case Counter::TEX_WORDS: return "TEX_WORDS";
	case Counter::TEX_ISSUES: return "TEX_ISSUES";
	case Counter::TI_POINTS: return "TI_POINTS";
	case Counter::TI_LINES: return "TI_LINES";
	case Counter::TI_TRIANGLES: return "TI_TRIANGLES";
	case Counter::TI_PRIM_VISIBLE: return "TI_PRIM_VISIBLE";
	case Counter::TI_PRIM_CULLED: return "TI_PRIM_CULLED";
	case Counter::TI_PRIM_CLIPPED: return "TI_PRIM_CLIPPED";
	case Counter::TI_FRONT_FACING: return "TI_FRONT_FACING";
	case Counter::TI_BACK_FACING: return "TI_BACK_FACING";
	case Counter::L2_READ_LOOKUP: return "L2_READ_LOOKUP";
	case Counter::L2_READ_HIT: return "L2_READ_HIT";
	case Counter::L2_READ_SNOOP: return "L2_READ_SNOOP";
	case Counter::L2_WRITE_LOOKUP: return "L2_WRITE_LOOKUP";
	case Counter::L2_WRITE_HIT: return "L2_WRITE_HIT";
	case Counter::L2_WRITE_SNOOP: return "L2_WRITE_SNOOP";
	case Counter::L2_EXT_READ_BEATS: return "L2_EXT_READ_BEATS";
	case Counter::L2_EXT_AR_STALL: return "L2_EXT_AR_STALL";
	case Counter::L2_EXT_WRITE_BEATS: return "L2_EXT_WRITE_BEATS";
	case Counter::L2_EXT_W_STALL: return "L2_EXT_W_STALL";
	default:
		assert( false && "Invalid counter" );
		return "INVALID_COUNTER";
	}
}

/// @return The last sampled value for the counter
counter_t PanfrostDataSource::get_counter( const Counter counter )
{
	auto names_begin = hardware_counters_mali_t86x;
	auto names_end = names_begin + count_counters();

	auto name_found = std::find_if(
		names_begin,
		names_end,
		[counter] ( const char* name ) {
			return std::strcmp( name + 5, to_string( counter ) ) == 0;
		}
	);

	if ( name_found == names_end )
	{
		PERFETTO_ELOG( "Cannot find %s", to_string( counter ) );
		return 0;
	}

	auto offset = name_found - names_begin;
	return counters[offset];
}

/// @todo Verify the logic behind this function
void PanfrostDataSource::trace_callback( TraceContext ctx )
{
	using namespace perfetto::protos::pbzero;

	auto state = ctx.GetIncrementalState();

	auto packet = ctx.NewTracePacket();
	packet->set_timestamp( perfetto::base::GetBootTimeNs().count() );

	auto event = packet->set_gpu_counter_event();
	event->set_gpu_id( 0 );

	GpuCounterDescriptor* desc = nullptr;

	if ( state->was_cleared )
	{
		/// @todo Do we need to send descriptions again
		/// when restarting this producer during a perfetto tracing session?
		desc = event->set_counter_descriptor();
	}

	uint32_t counter_id = 0;

	/// @brief Adds an int value to the current counter
	auto add_int = [desc, event, &counter_id] (
		const counter_t value,
		const std::string& name,
		GpuCounterDescriptor::MeasureUnit unit = GpuCounterDescriptor::NONE )
	{
		// Send description of the counter in first packet
		if ( desc )
		{
			auto spec = desc->add_specs();
			spec->set_counter_id( counter_id );
			std::string num = "00. ";
			std::sprintf( num.data(), "%02u", counter_id );
			num[2] = '.';
			spec->set_name( num + name );
			spec->add_numerator_units( unit );
		}
		else
		{
			auto counter = event->add_counters();
			counter->set_counter_id( counter_id );
			counter->set_int_value( value );
		}

		++counter_id;
	};

	/// @brief Adds a double value to the current counter
	auto add_double = [desc, event, &counter_id] (
		double value,
		const std::string& name,
		GpuCounterDescriptor::MeasureUnit unit = GpuCounterDescriptor::NONE )
	{
		// Send descriptor in first packet
		if ( desc )
		{
			auto spec = desc->add_specs();
			spec->set_counter_id( counter_id );
			std::string num = "00. ";
			std::sprintf( num.data(), "%02u", counter_id );
			num[2] = '.';
			spec->set_name( num + name );
			spec->add_numerator_units( unit );
		}
		else
		{
			auto counter = event->add_counters();
			counter->set_counter_id( counter_id );

			if ( unit == GpuCounterDescriptor::PERCENT )
			{
				value *= 100.0;
			}
			counter->set_double_value( value );
		}

		++counter_id;
	};

	auto gpu_active = get_counter( Counter::GPU_ACTIVE );
	add_int( gpu_active, "GPU active" );

	auto add_percentage = [&add_double]( counter_t numerator, counter_t denominator, const char* name )
	{
		double percentage = 0.0;
		if ( denominator >= 1.0 )
		{
			percentage = numerator / double( denominator );
		}
		add_double( percentage, name, GpuCounterDescriptor::PERCENT );
	};

	add_int( get_counter( Counter::JS0_ACTIVE ), "JS0 active" );
	add_int( get_counter( Counter::JS1_ACTIVE ), "JS1 active" );
	add_int( get_counter( Counter::JS2_ACTIVE ), "JS2 active" );

	add_int( get_counter( Counter::IRQ_ACTIVE ), "IRQ active" );

	auto js0_tasks = get_counter( Counter::JS0_TASKS );
	add_int( js0_tasks, "JS0 tasks");
	// Pixel count
	/// @todo Tile size
	/// 32x32 on Mali-T760, Mali-T800
	/// 16x16 on Mali-T600, Mali-T620, Mali-T720
	uint32_t tile_size = 32 * 32;
	add_int( get_counter( Counter::JS0_TASKS ) * 32 * 32, "Pixel count" );

	add_int( get_counter( Counter::JS1_TASKS ), "JS1 tasks" );
	add_int( get_counter( Counter::JS2_TASKS ), "JS2 tasks" );

	add_int( get_counter( Counter::FRAG_ACTIVE ), "Fragment active" );
	add_int( get_counter( Counter::COMPUTE_ACTIVE ), "Compute active" );

	auto tripipe_active = get_counter( Counter::TRIPIPE_ACTIVE );
	add_int( tripipe_active, "Tripipe active" );
	add_percentage( tripipe_active, gpu_active, "Tripipe usage" );

	// Compute tasks
	add_int( get_counter( Counter::COMPUTE_TASKS ), "Compute tasks" );
	// Compute threads
	add_int( get_counter( Counter::COMPUTE_THREADS ), "Compute threads" );

	// Primitives read from the tile list
	add_int( get_counter( Counter::FRAG_PRIMITIVES ), "Frag primitives" );
	// Primitives not relevant for the current tile
	add_int( get_counter( Counter::FRAG_PRIMITIVES_DROPPED ), "Frag primitives dropped" );
	// 2x2 pixel quad rasterized
	add_int( get_counter( Counter::FRAG_QUADS_RAST ), "Frag quad rasterized" );
	// 2x2 pixel quad early depth-stencil tested
	add_int( get_counter( Counter::FRAG_QUADS_EZS_TEST ), "Frag quads EZS test" );
	// 2x2 pixel quad killed by early depth-stencil test
	add_int( get_counter( Counter::FRAG_QUADS_EZS_KILLED ), "Frag quads EZS killed" );
	// ZS unit blocked for no physical tile memory available
	add_int( get_counter( Counter::FRAG_CYCLES_NO_TILE ), "Frag cycles no tile" );
	// Fragment thread created by the GPU
	add_int( get_counter( Counter::FRAG_THREADS ), "Frag threads" );
	// Threads with no sample coverage
	add_int( get_counter( Counter::FRAG_DUMMY_THREADS ), "Frag dummy threads" );

	// Threads triggering late ZS test
	add_int( get_counter( Counter::FRAG_THREADS_LZS_TEST ), "Frag threads LZS test" );
	// Threads killed by ZS test
	add_int( get_counter( Counter::FRAG_THREADS_LZS_KILLED ), "Frag threads LZS killed" );
	// 16x16 (or 32x32) pixel tiles rendered by shader core
	add_int( get_counter( Counter::FRAG_NUM_TILES ), "Frag num tiles" );
	// Pixel tile writeback cancelled due to maching hash
	add_int( get_counter( Counter::FRAG_TRANS_ELIM ), "Frag transaction eliminations" );

	// Arithmetic hardware
	auto arith_words = get_counter( Counter::ARITH_WORDS );
	add_int( arith_words, "Arithmetic words" );
	add_percentage( arith_words, tripipe_active, "Arithmetic usage" );

	// Architectural utilization of load/store pipe
	auto ls_words = get_counter( Counter::LS_WORDS );
	add_int( ls_words, "LS words" );
	add_percentage( ls_words, tripipe_active, "LS architectural usage" );

	// Microarchitectural utilization of load/store pipe
	auto ls_issues = get_counter( Counter::LS_ISSUES );
	add_int( ls_issues, "LS issues" );
	add_percentage( ls_issues, tripipe_active, "LS microarchitectural usage" );

	// Load/store cycles per instruction
	double ls_cpi = 0.0;
	if ( ls_words )
	{
		ls_cpi = ls_issues / double( ls_words );
	}
	add_double( ls_cpi, "LS pipe CPI" );

	// Load/Store cache events
	// Hitrate: percentage of hits vs total number of accesses

	// Read hitrate
	auto read_ops = get_counter( Counter::LSC_READ_OP );
	add_int( read_ops, "LSC reads" );
	add_percentage(
		get_counter( Counter::LSC_READ_HITS ),
		read_ops,
		"LSC read hitrate" );

	// Write hitrate
	auto write_ops = get_counter( Counter::LSC_WRITE_OP );
	add_int( write_ops, "LSC writes" );
	add_percentage(
		get_counter( Counter::LSC_WRITE_HITS ),
		write_ops,
		"LSC write hitrate" );

	// Atomic hitrate
	auto atomic_ops = get_counter( Counter::LSC_ATOMIC_OP );
	add_int( atomic_ops, "LSC atomics" );
	add_percentage(
		get_counter( Counter::LSC_ATOMIC_HITS ),
		atomic_ops,
		"LSC atomic hitrate" );

	// Lines fetched by L1 from L2
	add_int( get_counter( Counter::LSC_LINE_FETCHES ), "LSC lines fetched" );
	// Dirty lines evicted from L1 into L2
	add_int( get_counter( Counter::LSC_DIRTY_LINE ), "LSC dirty lines" );
	// Snoops into L1 from L2
	add_int( get_counter( Counter::LSC_SNOOPS ), "LSC snoops" );
	
	// Texture instruction executed
	auto tex_words = get_counter( Counter::TEX_WORDS );
	add_int( tex_words, "Texture instructions" );

	// Texture issue cycles used
	auto tex_issues = get_counter( Counter::TEX_ISSUES );
	add_int( tex_issues, "Texture issue cycles" );

	// Texture pipe CPI
	double tex_cpi = 0.0;
	if ( tex_words )
	{
		tex_cpi = tex_issues / double( tex_words );
	}
	add_double( tex_cpi, "Texture pipe CPI" );

	// Primitives
	add_int( get_counter( Counter::TI_POINTS ), "Point primitives" );
	add_int( get_counter( Counter::TI_LINES ), "Line primitives" );
	add_int( get_counter( Counter::TI_TRIANGLES ), "Triangle primitives" );

	// Visibility and culling
	add_int( get_counter( Counter::TI_PRIM_VISIBLE ), "Primitives visible" );
	add_int( get_counter( Counter::TI_PRIM_CULLED ), "Primitives culled" );
	add_int( get_counter( Counter::TI_PRIM_CLIPPED ), "Primitives clipped" );
	add_int( get_counter( Counter::TI_FRONT_FACING ), "Front-facing triangles" );
	add_int( get_counter( Counter::TI_BACK_FACING ), "Back-facing triangles" );

	// L2 cache
	auto read_lookup = get_counter( Counter::L2_READ_LOOKUP );
	add_int( read_lookup, "L2 read lookups" );
	add_percentage(
		get_counter( Counter::L2_READ_HIT ),
		read_lookup,
		"L2 read hitrate" );
	add_int( get_counter( Counter::L2_READ_SNOOP ), "L2 read snoops" );

	auto write_lookup = get_counter( Counter::L2_WRITE_LOOKUP );
	add_int( write_lookup, "L2 write lookups" );
	add_percentage(
		get_counter( Counter::L2_WRITE_HIT ),
		write_lookup,
		"L2 write hitrate" );
	add_int( get_counter( Counter::L2_WRITE_SNOOP ), "L2 write snoops" );

	/// @todo Some implementations use 8 bytes interfaces instead of 16
	uint32_t l2_axi_width_bytes = 16;
	/// @todo RK3399 has a Mali T860 MP4 (quad-core)
	uint32_t l2_axi_port_count = 4;

	auto read_beats = get_counter( Counter::L2_EXT_READ_BEATS );
	add_int(
		read_beats * l2_axi_width_bytes,
		"L2 external reads",
		GpuCounterDescriptor::BYTE );
	add_percentage(
		read_beats,
		l2_axi_port_count * gpu_active,
		"L2 external read usage" );
	add_int( get_counter( Counter::L2_EXT_AR_STALL ), "L2 external read stalls" );

	auto write_beats = get_counter( Counter::L2_EXT_WRITE_BEATS );
	add_int(
		write_beats * l2_axi_width_bytes,
		"L2 external writes",
		GpuCounterDescriptor::BYTE );
	add_percentage(
		write_beats,
		l2_axi_port_count * gpu_active,
		"L2 external write usage" );
	add_int( get_counter( Counter::L2_EXT_W_STALL ), "L2 external write stalls" );

	// At the end mark first packet sent
	state->was_cleared = false;
}

/// @brief Samples performance counters which means
/// asking the GPU to dump them to a user space buffer
void PanfrostDataSource::sample()
{
	// Dump performance counters to buffer
	drm_panfrost_perfcnt_dump dump = {};
	dump.buf_ptr = reinterpret_cast<uintptr_t>( counters );

	auto res = ioctl( card_fd, DRM_IOCTL_PANFROST_PERFCNT_DUMP, &dump );
	if ( !check( res, "Cannot dump" ) )
	{
		PERFETTO_ELOG( "Skipping sample" );
		return;
	}

	// Send packet to the tracing service
	Trace( trace_callback );
}

} // namespace pps::gpu
