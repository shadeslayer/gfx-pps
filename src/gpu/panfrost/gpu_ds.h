/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <perfetto.h>

namespace pps::gpu
{

enum class Counter
{
	// Job manager
	GPU_ACTIVE,
	JS0_ACTIVE,
	JS1_ACTIVE,
	JS2_ACTIVE,
	IRQ_ACTIVE,
	JS0_TASKS,
	JS1_TASKS,
	JS2_TASKS,

	// Shader core
	FRAG_ACTIVE,
	COMPUTE_ACTIVE,
	TRIPIPE_ACTIVE,
	COMPUTE_TASKS,
	COMPUTE_THREADS,

	// Frag frontend
	FRAG_PRIMITIVES,
	FRAG_PRIMITIVES_DROPPED,
	FRAG_QUADS_RAST,
	FRAG_QUADS_EZS_TEST,
	FRAG_QUADS_EZS_KILLED,
	FRAG_CYCLES_NO_TILE,
	FRAG_THREADS,
	FRAG_DUMMY_THREADS,

	// Frag backend
	FRAG_THREADS_LZS_TEST,
	FRAG_THREADS_LZS_KILLED,
	FRAG_NUM_TILES,
	FRAG_TRANS_ELIM,

	// Arithmetic pipe events
	ARITH_WORDS,

	// Load/Store pipe events
	LS_WORDS,
	LS_ISSUES,

	// Load/Store cache events
	LSC_READ_HITS,
	LSC_READ_OP,
	LSC_WRITE_HITS,
	LSC_WRITE_OP,
	LSC_ATOMIC_HITS,
	LSC_ATOMIC_OP,
	LSC_LINE_FETCHES,
	LSC_DIRTY_LINE,
	LSC_SNOOPS,

	// Texture pipe events
	TEX_WORDS,
	TEX_ISSUES,

	// Tiler primitive occurrence
	TI_POINTS,
	TI_LINES,
	TI_TRIANGLES,

	// Tiler visibility and culling occurence
	TI_PRIM_VISIBLE,
	TI_PRIM_CULLED,
	TI_PRIM_CLIPPED,
	TI_FRONT_FACING,
	TI_BACK_FACING,

	// L2 cache internal traffic events
	L2_READ_LOOKUP,
	L2_READ_HIT,
	L2_READ_SNOOP,
	L2_WRITE_LOOKUP,
	L2_WRITE_HIT,
	L2_WRITE_SNOOP,

	// L2 cache external traffic events
	L2_EXT_READ_BEATS,
	L2_EXT_AR_STALL,
	L2_EXT_WRITE_BEATS,
	L2_EXT_W_STALL,
};

using counter_t = uint32_t;

struct PanfrostIncrementalState
{
	bool was_cleared = true;
};

struct PanfrostDataSourceTraits : public perfetto::DefaultDataSourceTraits
{
	using IncrementalStateType = PanfrostIncrementalState;
};

class PanfrostDataSource
: public perfetto::DataSource<PanfrostDataSource, PanfrostDataSourceTraits>
{
  public:
	enum class State
	{
		Stop, // initial state, or stopped by the tracing service
		Start // running, sampling data
	};

	static constexpr const char* get_name() { return "gpu.metrics"; }

	void OnSetup( const SetupArgs& args ) override;
	void OnStart( const StartArgs& args ) override;
	void OnStop( const StopArgs& args ) override;

	static State get_state();

	/// Called in a loop, samples data from the GPU
	static void sample();

	/// @return The value of the counter c
	static counter_t get_counter( Counter c );

  private:
	static void trace_callback( TraceContext ctx );

	static counter_t* counters;

	static constexpr const char* card_path = "/dev/dri/card1";
	static int card_fd;

	std::allocator<counter_t> allocator;
};

}  // namespace pps::gpu
