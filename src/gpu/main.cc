/*
 * Copyright (c) 2019-2020 Collabora, Ltd.
 * Author: Antonio Caggiano <antonio.caggiano@collabora.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <thread>

#include "panfrost/gpu_ds.h"


int main( int argc, const char** argv )
{
	using namespace pps;

	// Connects to the system tracing service
	perfetto::TracingInitArgs args;
	args.backends = perfetto::kSystemBackend;
	perfetto::Tracing::Initialize( args );

	perfetto::DataSourceDescriptor dsd;
	dsd.set_name( gpu::PanfrostDataSource::get_name() );
	gpu::PanfrostDataSource::Register(dsd);

	while ( true )
	{
		switch ( gpu::PanfrostDataSource::get_state() )
		{
			using namespace std::chrono_literals;

			case gpu::PanfrostDataSource::State::Stop:
			{
				// Just wait until it starts
				std::this_thread::sleep_for( 2ms );
				break;
			}
			case gpu::PanfrostDataSource::State::Start:
			{
				gpu::PanfrostDataSource::sample();
				/// @todo Can this time be specified by a perfetto config file?
				std::this_thread::sleep_for( 1s );
				break;
			}
			default:
			{
				assert( false && "Invalid PanfrostDataSource state" );
			}
		}
	}

	return EXIT_SUCCESS;
}
