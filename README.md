Graphics Perfetto producers
---------------------------

This project contains a collection of graphics-related [Perfetto producers](https://perfetto.dev/#/architecture.md). A producer is a client process for the Perfetto tracing service. Currently under development are the following producers:

- Panfrost performance counters
- Weston timeline debug stream

## Build

This section guides you through the building process assuming you are compiling on Ubuntu.

1. Install dependencies:

```sh
apt install build-essential linux-libc-dev libtool autoconf cmake python libwayland-dev rapidjson-dev
```

2. Generate the CMake project:

```sh
cmake -S. -Bbuild
```

3. Compile (it may take a while):

```sh
cmake --build build
```

### Cross-compile

To cross-compile with cmake, you need to take these steps:

1. Prepare a chroot for cross-building

```sh
sudo debootstrap --variant=buildd focal /src/target/focal
sudo chroot /src/target/focal
mount -t proc proc /proc
```

2. Add arm64 to `/etc/apt/souces.list`

```
deb [arch=amd64] http://archive.ubuntu.com/ubuntu focal main
deb [arch=arm64] http://ports.ubuntu.com/ubuntu-ports focal main
```

3. Enable multiarch by adding arm64

```sh
dpkg --add--architecture arm64
```

4. Install the arm64 toolchain and other required packages:

```sh
apt update
apt install libc6-arm64-cross gcc-aarch64-linux-gnu g++-aarch64-linux-gnu cmake git libwayland-dev:arm64
```

5. Generate the project enabling the following option `CROSS_COMPILE_ARM64`:

```sh
cmake -S. -Bbuild/arm64 -DCROSS_COMPILE_ARM64=ON
```

6. Compile specifying the producers as targets

```sh
cmake --build build/arm64 --target producer-gpu producer-weston
```

## Run

To capture a trace with perfetto you need to take the following steps:

1. Create a [trace config](https://perfetto.dev/#/trace-config.md), which is a json formatted text file with extension `.cfg`, or use one of those under the `script` directory.

2. Copy the config file to `dep/perfetto/test/configs`. Under this directory you can also find more example of trace configs.

3. Change directory to `dep/perfetto` and run a [convenience script](https://perfetto.dev/#/running.md) to start the tracing service:

```sh
cd dep/perfetto
CONFIG=test.cfg OUT=out/build ./tools/tmux
```

4. Start other producers you may need, like `producer-gpu` or `producer-weston`.

5. Start perfetto under the tmux session initiated in step 3.

6. Once tracing has finished, you can detach from tmux with <kbd>ctrl</kbd>+<kbd>b</kbd>, <kbd>d</kbd>, and the convenience script should automatically copy the trace files into `$HOME/Downloads`.

7. Go to [ui.perfetto.dev](https://ui.perfetto.dev) and upload `$HOME/Downloads/trace.protobuf` by clicking on *Open trace file*.

### GPU producer

The GPU producer contains at the current state a data-source able to query GPU performance counters using the Panfrost driver. The data-source uses unstable ioctls that behave correctly on kernel version [5.4.23+](https://lwn.net/Articles/813601/) and [5.5.7+](https://lwn.net/Articles/813600/). To run the producer, follow these two simple steps:

1. Enable Panfrost unstable ioctls via kernel parameter:

```sh
modprobe panfrost unstable_ioctls=1
```

2. Run the producer:

```sh
./build/producer-gpu
```

### WIP: Weston producer

The weston producer needs the debug protocol to be present in weston. If the debug protocol is not present, update weston or install the latest development version from git.

1. Before running `producer-weston`, make sure weston is started with the `--debug` argument:

```sh
weston --debug
```

2. Execute the producer:

```sh
./build/producer-weston
```

## Troubleshooting

If the convenience script `tools/tmux` keeps copying artifacts to your `SSH_TARGET` without starting the tmux session, make sure you have `tmux` installed in your system.

```sh
apt install tmux
```
