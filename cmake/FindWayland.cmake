# Copyright (c) 2020 Collabora, Ltd.
# Author: Antonio Caggiano <antonio.caggiano@collabora.com>
#
# SPDX-License-Identifier: GPL-2.0-only

find_path( WAYLAND_CLIENT_INCLUDE_DIR
	NAMES wayland-client.h )

find_library( WAYLAND_CLIENT_LIBRARY
	NAMES wayland-client libwayland-client )

if( WAYLAND_CLIENT_INCLUDE_DIR AND WAYLAND_CLIENT_LIBRARY )
	add_library( wayland-client SHARED IMPORTED )
	set_target_properties( wayland-client PROPERTIES
		INTERFACE_INCLUDE_DIRECTORIES ${WAYLAND_CLIENT_INCLUDE_DIR}
		IMPORTED_LOCATION ${WAYLAND_CLIENT_LIBRARY} )
	set( Wayland_FOUND TRUE )
else()
	message( FATAL_ERROR "Could NOT find Wayland" )
endif()
