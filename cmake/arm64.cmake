# Copyright (c) 2020 Collabora, Ltd.
# Author: Antonio Caggiano <antonio.caggiano@collabora.com>
#
# SPDX-License-Identifier: GPL-2.0-only

# Cross compile for Linux arm64
message( STATUS "Generating for arm64" )
set( CMAKE_SYSTEM_NAME Linux )
set( CMAKE_SYSTEM_PROCESSOR arm )

set( CMAKE_C_COMPILER /usr/bin/aarch64-linux-gnu-gcc )
set( CMAKE_CXX_COMPILER /usr/bin/aarch64-linux-gnu-g++ )
