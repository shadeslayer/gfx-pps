# Copyright (c) 2019-2020 Collabora, Ltd.
# Author: Aguado Puig, Quim <quim.aguado@collabora.com>
# Author: Antonio Caggiano <antonio.caggiano@collabora.com>
#
# SPDX-License-Identifier: GPL-2.0-only

#!/bin/bash
if ! command -v python > /dev/null; then
	echo "[ERROR] Python needed to install Perfetto."
	exit -1
fi

# Fetch perfetto and weston
git submodule update --init --recursive

# Generate cmake project and compile
cmake -S. -Bbuild/arm64 \
	-DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
	-DCROSS_COMPILE_ARM64=ON \
	&& cmake --build build/arm64 --target producer-gpu -- -j4
